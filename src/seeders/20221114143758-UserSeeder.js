'use strict';
const bcrypt = require('bcrypt');

module.exports = {
    async up (queryInterface, Sequelize) {
        await queryInterface.bulkInsert('Users', [
            {
                name: 'Beatrix 1',
                email: 'beatrixmys1@gmail.com',
                password: await bcrypt.hash('password', 10),
                createdAt: new Date(),
                updatedAt: new Date(),
            }], {});
    },

    async down (queryInterface, Sequelize) {
        await queryInterface.bulkDelete('People', null, {});
    }
};
