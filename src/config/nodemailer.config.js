const nodemailer = require('nodemailer');
const { google } = require('googleapis');

const OAuth2Client = new google.auth.OAuth2(
    process.env.GOOGLE_CLIENT_ID,
    process.env.GOOGLE_CLIENT_SECRET,
    process.env.GOOGLE_REDIRECT_URI
);

OAuth2Client.setCredentials({ refresh_token: process.env.GOOGLE_REFRESH_TOKEN });

module.exports = {
    sendEmail: async (to, subject, template) => {
        try {
            const accessToken = await OAuth2Client.getAccessToken();

            const transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    type: 'OAuth2',
                    user: process.env.GOOGLE_EMAIL,
                    clientId: process.env.GOOGLE_CLIENT_ID,
                    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
                    refreshToken: process.env.GOOGLE_REFRESH_TOKEN,
                    accessToken: accessToken
                }
            });

            const mailOptions = {
                to,
                subject,
                html: template
            }

            console.log('Attempt sent email.');
            const info = await transporter.sendMail(mailOptions);
            console.log('Message Sent: %s', info.messageId);
        } catch (e) {
            console.log(e);
        }
    }
};
