const { User } = require('../models');
const bcrypt = require('bcrypt');
const { apiResponse } = require("../utils/apiResponse.utils");

module.exports = {
    login: async (req) => {
        try {
            const { email, password } = req.body;
            const user = await User.findOne({ where: { email } });
            if (!user) throw { code: 400, status: 'BAD_REQUEST', message: 'Invalid email or password' };

            const isMatchPassword = await bcrypt.compare(password, user.password);
            if (!isMatchPassword) throw { code: 400, status: 'BAD_REQUEST', message: 'Invalid email or password' };

            user.password = undefined;

            return apiResponse(200, 'OK', 'Success login.', { user });
        } catch (e) {
            throw apiResponse(e.code || 500, e.status || 'INTERNAL_SERVER_ERROR', e.message);
        }
    }
}
