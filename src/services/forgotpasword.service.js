const { User, PasswordReset } = require('../models');
const { StatusCodes: status } = require('http-status-codes');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const { apiResponse } = require('../utils/apiResponse.utils');
const { sendEmail } = require("../config/nodemailer.config");
const path = require('path');
const ejs = require('ejs');

module.exports = {
    forgot: async (req) => {
        try {
            const { email } = req.body;
            const user = await User.findOne({ where: { email } });
            if (!user) throw { code: status.NOT_FOUND, status: 'NOT_FOUND', message: 'User not found' };

            const passwordResetToken = await PasswordReset.findOne({ where: { email: user.email } });
            if (passwordResetToken) await passwordResetToken.destroy();

            const token = crypto.randomBytes(32).toString('hex');
            const hashToken = await bcrypt.hash(token, 10);

            await PasswordReset.create({
                email: user.email,
                token: hashToken,
                tokenExpires: new Date(Date.now() + 3600000)
            });

            const data = {
                name: user.name,
                email: user.email,
                token
            };
            const resetPasswordTemplate = await ejs.renderFile(path.join(__dirname, '../templates/forgot-password.ejs'), { data });
            sendEmail(
                user.email,
                'Reset Password',
                resetPasswordTemplate
            );

            return apiResponse(status.OK, 'OK', 'Forgot Password link has been sent.');
        } catch (e) {
            throw apiResponse(e.code || status.INTERNAL_SERVER_ERROR, e.status || 'INTERNAL_SERVER_ERROR', e.message);
        }
    },
    reset: async (req) => {
        try {
            const { token, email } = req.query;
            if (!token || !email) throw { code: status.BAD_REQUEST, status: 'BAD_REQUEST', message: 'Token and email are required.' };

            const user = await User.findOne({ where: { email } });
            if (!user) throw { code: status.NOT_FOUND, status: 'NOT_FOUND', message: 'User not found' };

            const passwordResetToken = await PasswordReset.findOne({ where: { email: user.email } });
            if (!passwordResetToken) throw { code: status.BAD_REQUEST, status: 'BAD_REQUEST', message: 'Invalid or expired reset password token' };
            if (passwordResetToken.tokenExpires < new Date()) {
                await passwordResetToken.destroy();
                throw { code: status.BAD_REQUEST, status: 'BAD_REQUEST', message: 'Invalid or expired reset password token' };
            }

            const isTokenValid = await bcrypt.compare(token, passwordResetToken.token);
            if (!isTokenValid) throw { code: status.BAD_REQUEST, status: 'BAD_REQUEST', message: 'Invalid or expired reset password token' };

            const { new_password, confirm_new_password } = req.body;
            if (!new_password || !confirm_new_password) throw { code: status.BAD_REQUEST, status: 'BAD_REQUEST', message: 'New password and confirm new password are required.' };
            if (new_password !== confirm_new_password) throw { code: status.BAD_REQUEST, status: 'BAD_REQUEST', message: 'New password and confirm new password do not match.' };

            const hashedPassword = await bcrypt.hash(new_password, 10);
            await user.update({ password: hashedPassword });
            await passwordResetToken.destroy();

            return apiResponse(status.OK, 'OK', 'Success reset password.');
        } catch (e) {
            throw apiResponse(e.code || status.INTERNAL_SERVER_ERROR, e.status || 'INTERNAL_SERVER_ERROR', e.message);
        }
    }
};
