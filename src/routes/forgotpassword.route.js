const express = require('express');
const ForgotPasswordController = require('../controllers/forgotpassword.controller');

const router = express.Router();

router.post('/forgot-password', ForgotPasswordController.forgot);
router.post('/reset-password', ForgotPasswordController.reset);

module.exports = router;
