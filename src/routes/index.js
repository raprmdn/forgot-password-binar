const express = require('express');
const ForgotPasswordRouter = require('./forgotpassword.route');
const AuthRouter = require('./auth.route');

const router = express.Router();

router.use('/', ForgotPasswordRouter);
router.use('/', AuthRouter);

module.exports = router;
