const ForgotPasswordService = require('../services/forgotpasword.service');

module.exports = {
    forgot: async (req, res) => {
        try {
            const serviceResponse = await ForgotPasswordService.forgot(req);
            return res.status(serviceResponse.code).json(serviceResponse);
        } catch (e) {
            return res.status(e.code).json(e);
        }
    },
    reset: async (req, res) => {
        try {
            const serviceResponse = await ForgotPasswordService.reset(req);
            return res.status(serviceResponse.code).json(serviceResponse);
        } catch (e) {
            return res.status(e.code).json(e);
        }
    }
};
