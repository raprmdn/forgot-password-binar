const AuthService = require('../services/auth.service');

module.exports = {
    login: async (req, res) => {
        try {
            const serviceResponse = await AuthService.login(req);
            return res.status(serviceResponse.code).json(serviceResponse);
        } catch (e) {
            return res.status(e.code).json(e);
        }
    },
}
